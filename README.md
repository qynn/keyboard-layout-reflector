# Keyboard Layout Reflector

Basic Elm app to generate a tilted reflection of a Keyboard Layout.

![example](export/default-mirrored.png)

## Install

0. Install [Elm](https://guide.elm-lang.org/install/elm.html)
1. Run `elm reactor`
2. Visit http://localhost:8000


## Usage

0. Create keyboard layout using [Keyboard Layout Editor](http://www.keyboard-layout-editor.com).
2. Use integers in the form `<row><column>` to label keys (only the last label in the labels array is parsed)
3. Download JSON
4. Run [KLE converter](https://keyboard-tools.xyz/kle-converter) from *adamws* [Keyboard tools](https://github.com/adamws/keyboard-tools) to format the serialized data
5. Select converted JSON using the `Browse` button
6. Adjust the tilt angle using `j`/`J` (`-`) and `k`/`K` (`+`)
7. Adjust the mirror offset using `h`/`H` (`-`) and `l`/`L` (`+`)
8. Click on `Export` to download the reflection JSON
