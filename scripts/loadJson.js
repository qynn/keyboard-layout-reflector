
function loadJson(app) {

    // https://web.dev/read-files/

    const fileSelector = document.getElementById('file-selector');
    fileSelector.addEventListener('change', (event) => {
        const file = event.target.files[0];

        if (file.type && file.type.startsWith("application/json")) {
            const reader = new FileReader();
            reader.addEventListener('load', (event) => {
                var data = event.target.result;
                app.ports.newLayout.send(data);
            });
            reader.readAsText(file);
        }

    });

}
