
function exportJson(json){

    let data = "data:text/json;charset=utf-8," + encodeURIComponent(json);
    let name = JSON.parse(json)['name']
    let dwnldAnchor = document.getElementById('download');
    dwnldAnchor.setAttribute("href", data);
    dwnldAnchor.setAttribute("download", name + '.json');
    dwnldAnchor.click();

    console.log("Exported data", json)

}
