port module Output exposing (..)

import Json.Encode as JE
import Layout exposing (Key, Model)


layoutValue : Layout.Model -> JE.Value
layoutValue layout =
    JE.object
        [ ( "name", JE.string layout.name )
        , ( "keys", JE.list keyValue layout.keys )
        ]


keyValue : Key -> JE.Value
keyValue key =
    JE.object
        [ ( "labels", JE.list JE.string <| [ Layout.getKeyLabel key ] )
        , ( "x", JE.float key.x )
        , ( "y", JE.float key.y )
        , ( "width", JE.float key.width )
        , ( "height", JE.float key.height )
        , ( "rotation_x", JE.float key.rotation_x )
        , ( "rotation_y", JE.float key.rotation_y )
        , ( "rotation_angle", JE.float key.rotation_angle )
        ]


encodeLayout : Layout.Model -> String
encodeLayout layout =
    JE.encode 4 <| layoutValue layout


port exportJson : String -> Cmd msg


exportLayout : Layout.Model -> Cmd msg
exportLayout layout =
    encodeLayout layout |> exportJson
