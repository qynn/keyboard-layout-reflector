module Mirror exposing (..)

import Dict exposing (Dict)
import Html exposing (Html, button, div, input, text)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Input exposing (Msg(..))
import Layout exposing (Key, Model, getKeyId, rotateKey)


type alias Param =
    { key : String
    , label : String
    , text : String
    , value : Float
    , incr : Float
    }


type alias Model =
    { tilt_x : Float
    , tilt_y : Float
    , tilt_angle : Param
    , offset : Param
    }


init : Model
init =
    { tilt_x = 2
    , tilt_y = 0
    , tilt_angle =
        { key = "tilt"
        , label = "Tilt (deg)"
        , text = "15"
        , value = 15
        , incr = 1
        }
    , offset =
        { key = "offset"
        , label = "Offset (U)"
        , text = "1"
        , value = 1
        , incr = 0.1
        }
    }


type Msg
    = SetParam Param String
    | TweakParam Param Float
    | InputKey Char


roundFloat : Int -> Float -> Float
roundFloat decimals f =
    let
        m =
            10 ^ toFloat decimals
    in
    toFloat (round (f * m)) / m


setParam : Model -> Param -> Float -> String -> Model
setParam model param value text =
    let
        updated_param =
            { param
                | value = value
                , text = text
            }
    in
    if param.key == model.tilt_angle.key then
        { model | tilt_angle = updated_param }

    else if param.key == model.offset.key then
        { model | offset = updated_param }

    else
        model


tweakParam : Model -> Param -> Float -> Model
tweakParam model param delta =
    let
        value =
            param.value + delta

        text =
            String.fromFloat value
    in
    setParam model param value text


tweak : Model -> Char -> Model
tweak model char =
    let
        ( maybeParam, df ) =
            case Char.toLower char of
                'j' ->
                    ( Just model.tilt_angle, -1 )

                'k' ->
                    ( Just model.tilt_angle, 1 )

                'h' ->
                    ( Just model.offset, -0.05 )

                'l' ->
                    ( Just model.offset, 0.05 )

                _ ->
                    ( Nothing, 0 )
    in
    case maybeParam of
        Just param ->
            let
                delta =
                    case Char.isUpper char of
                        True ->
                            5 * df

                        False ->
                            df
            in
            tweakParam model param delta

        Nothing ->
            model


updateTiltCenter : Layout.Model -> Model -> Model
updateTiltCenter layout model =
    case Layout.rightmostKey layout of
        Nothing ->
            model

        Just key ->
            { model
                | tilt_x = key.x
                , tilt_y = key.y
            }


update : Msg -> Model -> Model
update msg model =
    case msg of
        InputKey char ->
            tweak model char

        TweakParam param df ->
            tweakParam model param df

        SetParam param string ->
            case String.toFloat string of
                Just float ->
                    setParam model param float string

                Nothing ->
                    setParam model param 0 string


rotateKey : Model -> Key -> Key
rotateKey model key =
    Layout.rotateKey model.tilt_x model.tilt_y model.tilt_angle.value key


rotateLayout : Model -> Layout.Model -> Layout.Model
rotateLayout model layout =
    { layout
        | keys = List.map (rotateKey model) layout.keys
    }


mirrorKey : Model -> Int -> Key -> Key
mirrorKey model numRows key =
    let
        xm =
            model.tilt_x + model.offset.value

        a =
            degrees key.rotation_angle
    in
    let
        x =
            2 * xm - (key.x + cos a * key.width)

        y =
            key.y + sin a * key.width
    in
    { key
        | x = x
        , y = y
        , rotation_x = x
        , rotation_y = y
        , rotation_angle = 0 - key.rotation_angle
        , labels = [ getKeyId key + 10 * numRows |> String.fromInt ]
    }


mirrorLayout : Model -> Layout.Model -> Layout.Model
mirrorLayout model layout =
    let
        numRows =
            case List.maximum (List.map getKeyId layout.keys) of
                Just max ->
                    max // 10

                Nothing ->
                    0
    in
    { layout
        | keys = List.map (mirrorKey model numRows) layout.keys
    }


tiltDiv : Model -> Html msg
tiltDiv model =
    div [ style "padding" "5px" ]
        [ text <|
            String.concat
                [ "Tilt Center ("
                , model.tilt_x |> roundFloat 3 |> String.fromFloat
                , ", "
                , model.tilt_y |> roundFloat 3 |> String.fromFloat
                , ")"
                ]
        ]


paramDiv : Param -> Html Msg
paramDiv param =
    div [ style "padding" "5px" ]
        [ button
            [ style "width" "30px"
            , onClick <| TweakParam param param.incr
            ]
            [ text "+" ]
        , div
            []
            [ text <| param.label ++ " "
            , input
                [ placeholder ("<" ++ param.label ++ ">")
                , size 2
                , onInput <| SetParam param
                , value param.text
                ]
                []
            ]
        , button
            [ style "width" "30px"
            , onClick <| TweakParam param -param.incr
            ]
            [ text "-" ]
        ]
