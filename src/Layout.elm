port module Layout exposing (..)

import Json.Decode as JD


type alias Key =
    { x : Float
    , y : Float
    , width : Float
    , height : Float
    , rotation_x : Float
    , rotation_y : Float
    , rotation_angle : Float
    , labels : List String
    }


type alias Model =
    { name : String
    , keys : List Key
    }


nullLayout : Model
nullLayout =
    { name = "null"
    , keys = []
    }


init : String -> Model
init json =
    load json


type Msg
    = New String



-- HELPER FUNCTIONS


{-| Parse the last string in the labels array
-}
getKeyLabel : Key -> String
getKeyLabel key =
    key.labels
        |> List.reverse
        |> List.head
        |> Maybe.withDefault ""


getKeyId : Key -> Int
getKeyId key =
    getKeyLabel key
        |> String.toInt
        |> Maybe.withDefault 0


rotateKey : Float -> Float -> Float -> Key -> Key
rotateKey xr yr a key =
    let
        x =
            xr
                + cos (degrees a)
                * (key.x - xr)
                - sin (degrees a)
                * (key.y - yr)

        y =
            yr
                + cos (degrees a)
                * (key.y - yr)
                + sin (degrees a)
                * (key.x - xr)
    in
    { key
        | x = x
        , y = y
        , rotation_x = x
        , rotation_y = y
        , rotation_angle = key.rotation_angle + a
    }


{-| Move rotation center to upper left key corner
-}
cleanRotation : Key -> Key
cleanRotation key =
    let
        rk =
            rotateKey key.rotation_x key.rotation_y key.rotation_angle key
    in
    { rk | rotation_angle = key.rotation_angle }


selectKey : (Key -> Key -> Key) -> List Key -> Maybe Key
selectKey selector =
    let
        f =
            \key1 acc ->
                case acc of
                    Nothing ->
                        Just key1

                    Just key2 ->
                        Just (selector key1 key2)
    in
    List.foldr f Nothing


fieldSelector : Order -> (Key -> comparable) -> (Key -> Key -> Key)
fieldSelector order field =
    \key1 key2 ->
        if compare (field key1) (field key2) == order then
            key1

        else
            key2


fieldMax : (Key -> Float) -> List Key -> Float
fieldMax field keys =
    case selectKey (fieldSelector GT field) keys of
        Just key ->
            field key

        Nothing ->
            0


rightmostKey : Model -> Maybe Key
rightmostKey model =
    let
        f key =
            key.x + cos (degrees key.rotation_angle) * key.width

        selector =
            \key1 key2 ->
                case compare (f key1) (f key2) of
                    GT ->
                        key1

                    _ ->
                        key2
    in
    selectKey selector model.keys



-- STRING FORMATTING


keyToString : Key -> String
keyToString key =
    String.concat
        [ String.fromInt (getKeyId key)
        , " ["
        , String.fromFloat key.x
        , ", "
        , String.fromFloat key.y
        , "]"
        ]


toString : Model -> String
toString model =
    String.concat
        [ model.name ++ " ("
        , List.length model.keys |> String.fromInt
        , " keys)"
        ]



-- JSON PARSING


labelDecoder : JD.Decoder String
labelDecoder =
    JD.oneOf [ JD.string, JD.null "" ]


keyDecoder : JD.Decoder Key
keyDecoder =
    JD.map8 Key
        (JD.field "x" JD.float)
        (JD.field "y" JD.float)
        (JD.field "width" JD.float)
        (JD.field "height" JD.float)
        (JD.field "rotation_x" JD.float)
        (JD.field "rotation_y" JD.float)
        (JD.field "rotation_angle" JD.float)
        (JD.field "labels" (JD.list labelDecoder))


keyListDecoder : JD.Decoder (List Key)
keyListDecoder =
    JD.field "keys" (JD.list keyDecoder)


parseKeys : String -> List Key
parseKeys json =
    let
        result =
            JD.decodeString keyListDecoder json
    in
    case result of
        Ok keys ->
            keys

        Err err ->
            let
                e =
                    Debug.log "parseKeys Error" json
            in
            []


parseName : String -> String
parseName json =
    let
        result =
            JD.decodeString (JD.at [ "meta", "name" ] JD.string) json
    in
    case result of
        Ok name ->
            if name == "" then
                "unknown"

            else
                name

        Err err ->
            "error"


load : String -> Model
load json =
    { name = parseName json
    , keys = List.map cleanRotation <| parseKeys json
    }



-- UPDATE
-- https://github.com/elm-community/js-integration-examples/blob/master/localStorage/src/Main.elm


port newLayout : (String -> msg) -> Sub msg


subscriptions : Model -> Sub Msg
subscriptions _ =
    newLayout New


update : Msg -> Model -> Model
update msg model =
    case msg of
        New json ->
            load json
