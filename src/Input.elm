module Input exposing
    ( Msg(..)
    , subscriptions
    )

import Browser.Events as BE
import Json.Decode as JD


type Msg
    = Key Char
    | Control String



{- https://github.com/elm/browser/blob/1.0.0/notes/keyboard.md -}


eventMapper : String -> Bool -> String -> Msg
eventMapper string shift code =
    case String.uncons string of
        Just ( char, "" ) ->
            Key char

        _ ->
            Control code


eventDecoder : JD.Decoder Msg
eventDecoder =
    JD.map3 eventMapper
        (JD.field "key" JD.string)
        (JD.field "shiftKey" JD.bool)
        (JD.field "code" JD.string)


subscriptions : Sub Msg
subscriptions =
    BE.onKeyDown eventDecoder
