module Render exposing (show)

import Html exposing (Html)
import Layout exposing (Key, cleanRotation, getKeyId)
import Mirror
import Svg exposing (..)
import Svg.Attributes exposing (..)


scale : Float
scale =
    50


unitToPx : Float -> String
unitToPx f =
    String.fromFloat (scale * f)


rotateAttr : Key -> String
rotateAttr key =
    String.concat
        [ "rotate("
        , String.fromFloat key.rotation_angle ++ ", "
        , unitToPx key.rotation_x ++ ", "
        , unitToPx key.rotation_y ++ ")"
        ]


renderKey : Key -> Svg msg
renderKey k =
    let
        rk =
            cleanRotation k
    in
    g
        [ stroke "black"
        , fill "none"
        ]
        [ rect
            [ x (unitToPx rk.x)
            , y (unitToPx rk.y)
            , width (unitToPx rk.width)
            , height (unitToPx rk.height)
            , rx (unitToPx 0.1)
            , ry (unitToPx 0.1)
            , transform (rotateAttr rk)
            ]
            []
        , text_
            [ fontFamily "Noto Sans"
            , fontSize (unitToPx 0.4)
            , x (unitToPx (rk.x + rk.width / 2 - 0.25))
            , y (unitToPx (rk.y + rk.height / 2 + 0.15))
            , transform (rotateAttr rk)
            ]
            [ text (String.fromInt (getKeyId rk)) ]
        ]


renderMirror : Mirror.Model -> Svg msg
renderMirror mirror =
    g
        [ stroke "none"
        , fill "red"
        ]
        [ rect
            [ x (unitToPx mirror.tilt_x)
            , y (unitToPx mirror.tilt_y)
            , width "4"
            , height "4"
            ]
            []
        , rect
            [ x (unitToPx (mirror.tilt_x + 2 * mirror.offset.value))
            , y (unitToPx mirror.tilt_y)
            , width "4"
            , height "4"
            ]
            []
        , rect
            [ x (unitToPx (mirror.tilt_x + mirror.offset.value))
            , y (unitToPx (mirror.tilt_y - 1))
            , width "1"
            , height (unitToPx 2)
            ]
            []
        ]


show : Mirror.Model -> List Key -> Html msg
show mirror keys =
    let
        w =
            unitToPx <| Layout.fieldMax .x keys + 6

        h =
            unitToPx <| Layout.fieldMax .y keys + 6

        px =
            unitToPx <| -2

        py =
            unitToPx <| -4
    in
    svg
        [ width w
        , height h
        , viewBox <|
            String.concat <|
                [ px ++ " "
                , py ++ " "
                , w ++ " "
                , h
                ]
        , Svg.Attributes.style "background: rgba(0, 0, 0, 0.1);"
        ]
        (List.append
            (List.map renderKey keys)
            [ renderMirror mirror ]
        )
