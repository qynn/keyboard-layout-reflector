module Main exposing (..)

import Browser
import Html exposing (Html, button, div, input, text)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Input
import Layout
import Mirror
import Output
import Render



-- MAIN


main : Program String Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { layout : Layout.Model
    , left : Layout.Model
    , right : Layout.Model
    , mirror : Mirror.Model
    }


init : String -> ( Model, Cmd Msg )
init flags =
    let
        layout =
            Layout.load flags
    in
    ( applyMirror
        { layout = layout
        , left = Layout.nullLayout
        , right = Layout.nullLayout
        , mirror = Mirror.updateTiltCenter layout Mirror.init
        }
    , Cmd.none
    )


type Msg
    = InputMsg Input.Msg
    | LayoutMsg Layout.Msg
    | MirrorMsg Mirror.Msg
    | DownloadJson



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Sub.map InputMsg Input.subscriptions
        , Sub.map LayoutMsg (Layout.subscriptions model.left)
        ]



-- UPDATE


applyMirror : Model -> Model
applyMirror model =
    let
        left =
            Mirror.rotateLayout model.mirror model.layout

        right =
            Mirror.mirrorLayout model.mirror left
    in
    { model
        | left = { left | name = model.layout.name ++ "-left" }
        , right = { right | name = model.layout.name ++ "-right" }
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        cmd =
            if msg == DownloadJson then
                Output.exportLayout
                    { name = model.layout.name ++ "-mirrored"
                    , keys = model.left.keys ++ model.right.keys
                    }

            else
                Cmd.none

        updatedModel =
            case msg of
                InputMsg inputMsg ->
                    case inputMsg of
                        Input.Key char ->
                            { model
                                | mirror =
                                    Mirror.update (Mirror.InputKey char) model.mirror
                            }

                        _ ->
                            model

                MirrorMsg mirrorMsg ->
                    { model
                        | mirror =
                            Mirror.update mirrorMsg model.mirror
                    }

                LayoutMsg layoutMsg ->
                    let
                        layout =
                            Layout.update layoutMsg model.layout
                    in
                    { model
                        | layout = layout
                        , mirror = Mirror.updateTiltCenter layout model.mirror
                    }

                DownloadJson ->
                    model
    in
    ( applyMirror updatedModel, cmd )



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ div
            [ style "border" "1px solid black"
            , style "margin" "10px 0"
            , style "padding" "10px"
            , style "width" "170px"
            ]
            [ div
                [ style "color" "red"
                , style "fontWeight" "bold"
                , style "margin" "5px"
                ]
                [ text <| Layout.toString model.layout
                ]
            , Mirror.tiltDiv model.mirror
            , Html.map MirrorMsg <| Mirror.paramDiv model.mirror.tilt_angle
            , Html.map MirrorMsg <| Mirror.paramDiv model.mirror.offset
            , button
                [ style "width" "130px"
                , style "margin" "5px"
                , onClick <| DownloadJson
                ]
                [ text "Export" ]
            ]
        , Render.show model.mirror <|
            model.left.keys
                ++ model.right.keys
        ]
