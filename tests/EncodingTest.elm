module EncodingTest exposing (suite)

import Expect exposing (..)
import Layout exposing (Key)
import Output exposing (..)
import Test exposing (..)


key11 : Key
key11 =
    { x = 1
    , y = 1
    , width = 1.5
    , height = 1
    , rotation_x = 0
    , rotation_y = 0
    , rotation_angle = 0
    , labels = [ "!", "", "11" ]
    }


key21 : Key
key21 =
    let
        key =
            Layout.rotateKey 1 1 90 key11
    in
    { key | labels = [ "21" ] }


layout : Layout.Model
layout =
    { name = "Output"
    , keys = [ key11, key21 ]
    }


output =
    """{
    "name": "Output",
    "keys": [
        {
            "labels": [
                "11"
            ],
            "x": 1,
            "y": 1,
            "width": 1.5,
            "height": 1,
            "rotation_x": 0,
            "rotation_y": 0,
            "rotation_angle": 0
        },
        {
            "labels": [
                "21"
            ],
            "x": 1,
            "y": 1,
            "width": 1.5,
            "height": 1,
            "rotation_x": 1,
            "rotation_y": 1,
            "rotation_angle": 90
        }
    ]
}"""


suite : Test
suite =
    describe "Serializing"
        [ test "Output.encodeLayout" <|
            \_ ->
                Output.encodeLayout layout
                    |> Expect.equal output
        ]
