module DecodingTest exposing (layout, suite)

import Expect exposing (..)
import Layout exposing (..)
import Test exposing (..)


json =
    """
      {
      "meta": {
          "author": "",
          "name": "Test"
      },
      "keys": [
        {
          "labels": ["!", null, null, null, "1"],
          "x": 1,
          "y": 1,
          "width": 1.5,
          "height": 1,
          "rotation_x": 1,
          "rotation_y": 1,
          "rotation_angle": 90
        },
        {
          "labels": ["2", "@"],
          "x": 1,
          "y": 0,
          "width": 1,
          "height": 1,
          "rotation_x": 0,
          "rotation_y": 0,
          "rotation_angle": 30
        }
      ]
      }
    """


key1 : Key
key1 =
    { x = 1
    , y = 1
    , width = 1.5
    , height = 1
    , rotation_x = 1
    , rotation_y = 1
    , rotation_angle = 90
    , labels = [ "!", "", "", "", "1" ]
    }


key2 : Key
key2 =
    let
        a =
            degrees 30
    in
    { x = cos a
    , y = sin a
    , width = 1
    , height = 1
    , rotation_x = cos a
    , rotation_y = sin a
    , rotation_angle = 30
    , labels = [ "2", "@" ]
    }


layout : Layout.Model
layout =
    { name = "Test"
    , keys = [ key1, key2 ]
    }


suite : Test
suite =
    describe "Json parsing"
        [ test "Layout.parseKeys" <|
            \_ ->
                parseKeys json
                    |> List.length
                    |> Expect.equal 2
        , test "Layout.getKeyId" <|
            \_ ->
                List.map getKeyId [ key1, key2 ]
                    |> Expect.equal [ 1, 0 ]
        , test "Layout.load" <|
            \_ ->
                load json
                    |> Expect.equal layout
        ]
